
### SES Package
---

    SES is a wrapper around Amazon's SES Go API (github.com/aws/aws-sdk-go). It provides the following functionality:

    * Send Email
    * Get Email Stats
    
    The following Environment Variables should be present in your environment:

    * AWS_ACCESS_KEY_ID
    * AWS_SECRET_ACCESS_KEY
    * AWS_DEFAULT_REGION

#### Mailer

Mailer is the exposed inteface type from the package. It is ussed to send email and get email stats. When creating a mailer an optional parameter of aws.Config can be passed. Otherwise the default aws.Config is used which reads its settings from the environment.

    m:= ses.NewMailer()

#### Sending Email

To send email, you create an instance of `ses.Mail` and populate it. Create a new `Mailer` and then call the `SendMail` method passing it the `ses.Mail` instance
    
    m := ses.Mail{
        HTML:    htmlTemplate,
        Text:    txtTempplate,
        Model:   model, // interface{}
        Subject: subject,
        To:      emailTo,
        From:    from,
        ReplyTo: from,
    }

    mailer := ses.NewMailer()

    id, err:=mailer.SendMail(m)
    if err != nil {
        // handle error
    }

    id - Will contain the SES Id assigned to that send mail transaction.

#### Get Send Quota

Returns the user's current sending limits. The api call returns an instance of `ses.Quota`

    s := NewMailer()
    q, err := s.GetSendQuota()
    if err != nil {
        // Handle Error
    }

    The Quota type is defined as:

    type Quota struct {
        Max24HourSend   float64
        MaxSendRate     float64
        SentLast24Hours float64
    }


#### Get Statistics

Returns the user's sending statistics. The result is a list of data points, representing the last two weeks of sending activity. Each data point in the list contains statistics for a 15-minute interval. This action is throttled at one request per second. The api call returns `ses.Stat slice`


    s := NewMailer()
    
    stats, err := s.GetStatistics()
    if err != nil {
        // Handle Error
    }

    The Stat type is defined as:

    type Stat struct {
        Bounces          int64
        Complaints       int64
        DeliveryAttempts int64
        Rejects          int64
        Timestamp        time.Time
    }
