package ses

import (
	"net/http"
	"text/template"
	"time"

	"bytes"

	"bitbucket.org/asante/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

// Mailer is the interface used to send email via SES
type Mailer interface {
	SendMail(m *Mail) (string, error)
	GetSendQuota() (*Quota, error)
	GetStatistics() ([]Stat, error)
}

// SES is the type that implements the mailer interface. It embeds the aws ses type.
type SES struct {
	*ses.SES
	Config *AWSConfig
}

// AWSConfig is the config for AWS SES
type AWSConfig struct {
	AWSAccessKey       string
	AWSSecretAccessKey string
	AWSDefaultRegion   string
}

// NewMailer creates a new mailer with the provided aws config. Config is optional
// if not passed in it will use the aws.DefaultConfig
func NewMailer(c AWSConfig) Mailer {
	// Create A new DefaultConfig to be concurrent.
	config := aws.Config{
		Credentials:             credentials.NewStaticCredentials(c.AWSAccessKey, c.AWSSecretAccessKey, ""),
		Endpoint:                aws.String(""),
		Region:                  &c.AWSDefaultRegion,
		DisableSSL:              aws.Bool(false),
		HTTPClient:              http.DefaultClient,
		LogLevel:                aws.LogLevel(0),
		MaxRetries:              aws.Int(aws.UseServiceDefaultRetries),
		DisableParamValidation:  aws.Bool(false),
		DisableComputeChecksums: aws.Bool(false),
		S3ForcePathStyle:        aws.Bool(false),
	}

	s := SES{
		SES:    ses.New(session.New(), &config),
		Config: &c,
	}

	return &s
}

// Mail holds all the data necessary to send out email.
type Mail struct {
	HTML    *template.Template
	Text    *template.Template
	Model   interface{}
	Subject string
	// This is not idiomatic but SES api requires it to be a slice
	// of string pointers. Review with team, do we abstract this?
	To      []*string
	From    string
	ReplyTo string
}

// SendMail sends out mail based on the pased in mail type.
func (s *SES) SendMail(m *Mail) (string, error) {
	log.Start("ses.SendMail")

	input, err := s.createEmail(m)
	if err != nil {
		log.CompleteErr(err, "ses.SendMail")
		return "", err
	}

	id, err := s.sendEmailRequest(input)
	if err != nil {
		log.CompleteErr(err, "ses.SendMail")
		return "", err
	}

	log.Complete("ses.SendMail")
	return id, nil
}

// createEmail creates the SendEmailInput type from the mail type.
func (s *SES) createEmail(m *Mail) (*ses.SendEmailInput, error) {
	var hBuffer, tBuffer bytes.Buffer

	// html
	if err := m.HTML.Execute(&hBuffer, m.Model); err != nil {
		log.Errf(err, "ses.createEmail", "Err while transforming HTML email")
		return nil, err
	}

	// text
	if err := m.Text.Execute(&tBuffer, m.Model); err != nil {
		log.Errf(err, "ses.createEmail", "Err while transforming Text email")
		return nil, err
	}

	html := string(hBuffer.Bytes())
	text := string(tBuffer.Bytes())

	if m.ReplyTo == "" {
		m.ReplyTo = m.From
	}

	params := ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: m.To,
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Html: &ses.Content{
					Data: &html,
				},
				Text: &ses.Content{
					Data: &text,
				},
			},
			Subject: &ses.Content{
				Data: &m.Subject,
			},
		},
		Source: &m.From,
	}

	log.Complete("ses.createEmail")
	return &params, nil
}

// sendEmailRequest send an email request via the ses service from amazon.
func (s *SES) sendEmailRequest(r *ses.SendEmailInput) (string, error) {
	log.Start("ses.sendEmailRequest")

	output, err := s.SES.SendEmail(r)
	if err != nil {
		log.Err(err, "ses.sendEmailRequest")
		return "", err
	}

	log.Complete("ses.sendEmailRequest")
	return *output.MessageId, nil
}

// Quota is the type that denotes the SES quotas.
type Quota struct {
	Max24HourSend   float64
	MaxSendRate     float64
	SentLast24Hours float64
}

// GetSendQuota gets the quota for the account in SES.
func (s *SES) GetSendQuota() (*Quota, error) {
	log.Start("ses.GetSendQuota")

	var input ses.GetSendQuotaInput
	output, err := s.SES.GetSendQuota(&input)
	if err != nil {
		log.CompleteErr(err, "ses.GetSendQuota")
		return nil, err
	}

	q := Quota{
		Max24HourSend:   *output.Max24HourSend,
		MaxSendRate:     *output.MaxSendRate,
		SentLast24Hours: *output.SentLast24Hours,
	}

	log.Complete("ses.GetSendQuota")
	return &q, nil
}

// Stat represents a stat from SES.
type Stat struct {
	Bounces          int64
	Complaints       int64
	DeliveryAttempts int64
	Rejects          int64
	Timestamp        time.Time
}

// GetStatistics gets the statistics from the email system.
func (s *SES) GetStatistics() ([]Stat, error) {
	log.Start("ses.GetStatistics")
	var input ses.GetSendStatisticsInput
	output, err := s.SES.GetSendStatistics(&input)
	if err != nil {
		log.CompleteErr(err, "ses.GetStatistics")
		return nil, err
	}

	var stats []Stat

	for _, item := range output.SendDataPoints {
		s := Stat{
			Bounces:          *item.Bounces,
			Complaints:       *item.Complaints,
			DeliveryAttempts: *item.DeliveryAttempts,
			Rejects:          *item.Rejects,
			Timestamp:        *item.Timestamp,
		}
		stats = append(stats, s)
	}

	log.Complete("ses.GetStatistics")
	return stats, nil
}
