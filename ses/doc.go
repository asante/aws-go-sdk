// Package ses is a wrapper around Amazon's SES Go API (github.com/aws/aws-sdk-go). It provides the following functionality:
//
//    * Send Email
//    * Get Email Stats
//
//   The following Environment Variables should be present in your environment:
//
//    * AWS_ACCESS_KEY_ID
//    * AWS_SECRET_ACCESS_KEY
//    * AWS_DEFAULT_REGION
package ses
