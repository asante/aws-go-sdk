package ses

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"text/template"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/service/ses"
)

const (
	h      = `<html><body><h1>hello</h1><h2>{{.Name}}</h2></body></html>`
	txt    = `hello {{.Name}}`
	from   = "sestest@wreckingballmedia.com"
	to     = "success@simulator.amazonses.com"
	bounce = "bounce@simulator.amazonses.com"
)

type Model struct {
	Name string
}

func TestMessageCreate(t *testing.T) {
	expecHTML := "<html><body><h1>hello</h1><h2>World</h2></body></html>"
	expecTxt := "hello World"

	htmlTemp, err := template.New("test").Parse(h)
	if err != nil {
		t.Fatalf("Error parsing html template, %+v", err)
	}
	txtTemp, err := template.New("testText").Parse(txt)
	if err != nil {
		t.Fatalf("Error parsing txt template, %+v", err)
	}

	model := Model{"World"}
	to := to
	emailTo := []*string{&to}
	from := from
	subject := "test subject"

	m := Mail{
		HTML:    htmlTemp,
		Text:    txtTemp,
		Model:   model,
		Subject: subject,
		To:      emailTo,
		From:    from,
		ReplyTo: from,
	}

	c := loadConfig()

	config := aws.Config{
		Credentials:             credentials.NewStaticCredentials(c.AWSAccessKey, c.AWSSecretAccessKey, ""),
		Endpoint:                aws.String(""),
		Region:                  &c.AWSDefaultRegion,
		DisableSSL:              aws.Bool(false),
		HTTPClient:              http.DefaultClient,
		LogLevel:                aws.LogLevel(0),
		MaxRetries:              aws.Int(aws.UseServiceDefaultRetries),
		DisableParamValidation:  aws.Bool(false),
		DisableComputeChecksums: aws.Bool(false),
		S3ForcePathStyle:        aws.Bool(false),
	}

	s := &SES{ses.New(&config), &c}

	input, err := s.createEmail(&m)
	if err != nil {
		t.Fatalf("Error creating email, %+v", err)
	}
	// To Address
	if input.Destination.ToAddresses[0] != emailTo[0] {
		t.Errorf("Expected To Address to be %+v, got %+v", emailTo, input.Destination.ToAddresses)
	}

	// HTML
	if *input.Message.Body.Html.Data != expecHTML {
		t.Errorf("Expected HTML to be %+v, got %+v", expecHTML, *input.Message.Body.Html.Data)
	}

	// TEXT
	if *input.Message.Body.Text.Data != expecTxt {
		t.Errorf("Expected Text to be %+v, got %+v", expecTxt, *input.Message.Body.Text.Data)
	}

	// Subject
	if *input.Message.Subject.Data != subject {
		t.Errorf("Expected Subject to be %+v, got %+v", subject, *input.Message.Subject.Data)
	}

	if *input.Source != from {
		t.Errorf("Expected From to be %+v, got %+v", from, *input.Source)
	}
}

func TestMessageSend(t *testing.T) {
	htmlTemp, err := template.New("test").Parse(h)
	if err != nil {
		t.Fatalf("Error parsing html template, %+v", err)
	}
	txtTemp, err := template.New("testText").Parse(txt)
	if err != nil {
		t.Fatalf("Error parsing txt template, %+v", err)
	}
	model := Model{"World"}
	to := to
	from := from
	m := Mail{
		HTML:    htmlTemp,
		Text:    txtTemp,
		Model:   model,
		Subject: "test subject",
		To:      []*string{&to},
		From:    from,
		ReplyTo: from,
	}

	s := NewMailer(loadConfig())
	id, err := s.SendMail(&m)
	if err != nil {
		t.Fatalf("Error sending email, %+v", err)
	}

	if id == "" {
		t.Fatalf("Error sending email, expected Id to have value but it didn't")
	}
}

func TestFailMessageSend(t *testing.T) {
	htmlTemp, err := template.New("test").Parse(h)
	if err != nil {
		t.Fatalf("Error parsing html template, %+v", err)
	}

	txtTemp, err := template.New("testText").Parse(txt)
	if err != nil {
		t.Fatalf("Error parsing txt template, %+v", err)
	}

	model := Model{"World"}
	to := bounce
	from := from
	m := Mail{
		HTML:    htmlTemp,
		Text:    txtTemp,
		Model:   model,
		Subject: "test subject",
		To:      []*string{&to},
		From:    from,
		ReplyTo: from,
	}

	s := NewMailer(loadConfig())
	id, err := s.SendMail(&m)
	if err != nil {
		t.Fatalf("Error sending email, %+v", err)
	}

	if id == "" {
		t.Fatalf("Error sending email, expected Id to have value but it didn't")
	}
}

func TestGetSendQuota(t *testing.T) {
	s := NewMailer(loadConfig())
	q, err := s.GetSendQuota()
	if err != nil {
		t.Fatalf("err getting quota, %+v", err)
	}

	fmt.Println(*q)
	if q.Max24HourSend == 0 {
		t.Error("Max 24 hour send is 0")
	}

	if q.MaxSendRate == 0 {
		t.Error("Max send rate is 0")
	}
}

func TestGetStatistics(t *testing.T) {
	s := NewMailer(loadConfig())
	stats, err := s.GetStatistics()
	if err != nil {
		t.Fatalf("err getting quota, %+v", err)
	}

	fmt.Println(stats)
}

func loadConfig() AWSConfig {
	return AWSConfig{
		AWSAccessKey:       os.Getenv("AWS_ACCESS_KEY_ID"),
		AWSSecretAccessKey: os.Getenv("AWS_SECRET_ACCESS_KEY"),
		AWSDefaultRegion:   os.Getenv("AWS_DEFAULT_REGION"),
	}
}
