// Package sqs is a wrapper around Amazon's SQS Go API
// (github.com/aws/aws-sdk-go). It provides the following functionality:
//
//    * Send Messages to a Queue
//    * Consume/Recieve Messages from a Queue
package sqs
