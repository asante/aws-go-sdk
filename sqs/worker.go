package sqs

import (
	"errors"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"time"

	"bitbucket.org/asante/log"
)

// doWork is used internally to route work.
type doWork struct {
	msg    Message
	worker WorkFunc
}

// WorkFunc is the function that is called for a command.
type WorkFunc func(payload string) error

// work wraps the WorkFunc call in case there is a panic it is captured.
func (wf WorkFunc) do(payload string) (err error) {
	defer func() {
		if r := recover(); r != nil {
			// Capture the stack trace
			buf := make([]byte, 10000)
			runtime.Stack(buf, false)
			err2 := fmt.Errorf("PANIC Defered [%v] : Stack Trace : %v", r, string(buf))

			if err2 != nil {
				err = err2
			}

			return
		}
	}()

	err = wf(payload)
	return
}

type (

	// CmdConfig is the config used for the commander.
	CmdConfig struct {
		AWSConfig
		HPWorkers   int
		LPWorkers   int
		PostProcess int
	}

	// SchTask represents a scheduled task.
	SchTask struct {
		Work      WorkFunc
		Frequency int64 // 0-900 seconds (15min.)
	}

	// Commander handles the commanding of all the work related to messages
	Commander struct {
		commands  map[string]WorkFunc
		schtasks  map[string]SchTask
		hpWork    chan doWork // Unbuffered channel that work is sent into.
		lpWork    chan doWork
		response  chan workResponse // Unbuffered channel that handles any errors from work.
		shutdown  chan struct{}     // Unbuffered channel to signal for a goroutine to die.
		kill      chan struct{}     // Unbuffered channel to signal to kill everything.
		wg        sync.WaitGroup
		isRunning bool
		config    CmdConfig
	}
)

// NewCommander returns a new commander already initialized.
func NewCommander(cfg CmdConfig) (*Commander, error) {
	if err := cfg.AWSConfig.Valid(); err != nil {
		return nil, err
	}

	if cfg.HPWorkers == 0 {
		cfg.HPWorkers = 10
	}

	if cfg.LPWorkers == 0 {
		cfg.LPWorkers = 2
	}

	if cfg.PostProcess == 0 {
		cfg.PostProcess = 2
	}

	return &Commander{
		commands: make(map[string]WorkFunc),
		hpWork:   make(chan doWork),
		lpWork:   make(chan doWork),
		response: make(chan workResponse),
		shutdown: make(chan struct{}),
		kill:     make(chan struct{}),
		wg:       sync.WaitGroup{},
		config:   cfg,
	}, nil
}

// RegisterCmd register a WorkFunc for a specific command.
func (c *Commander) RegisterCmd(cmd string, f WorkFunc) {
	c.commands[cmd] = f
}

// RegisterSchTask register a WorkFunc for a specific command and it runs every X seconds.
// Max seconds is 900. Due to limitation of message delay in sqs.
func (c *Commander) RegisterSchTask(cmd string, f WorkFunc, frequency int64) {
	if frequency > 900 {
		frequency = 900
	}

	t := SchTask{Work: f, Frequency: frequency}
	c.schtasks[cmd] = t
}

// Start starts the commander to listen and process on queues.
func (c *Commander) Start() {
	log.Start("sqs.Commander.Start")
	c.isRunning = true

	// Run HighPriority Processor
	c.wg.Add(1)
	go c.run(HighPriority, c.config.HPWorkers, c.hpWork)

	// Run LowPriority Processor
	c.wg.Add(1)
	go c.run(LowPriority, c.config.LPWorkers, c.lpWork)

	// Launch Post Processors
	for i := 0; i < c.config.PostProcess; i++ {
		c.wg.Add(1)
		go c.postProcess()
	}

	c.wg.Add(1)
	go func() {
		defer func() {
			log.Complete("sqs.Commander.Start listeners...")
		}()
		// Create a channel to talk with the OS.
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, os.Interrupt)

		select {
		case <-sigChan:
			log.Info("sqs.Commander.Start", "os signal recieved shutting down")
			c.wg.Done()
			c.shutDown()
			return
		case <-c.shutdown:
			c.wg.Done()
			return
		}
	}()
}

// run runs the commander and starts the post processor
func (c *Commander) run(q int, workers int, workCh chan doWork) {
	defer func() {
		log.Complete("sqs.commander.run")
		c.wg.Done()
	}()

	log.Startf("sqs.commander.run", "starting up, q[%d], workers[%d]", q, workers)

	// Start up the Work Go Routines.
	for x := 0; x < workers; x++ {
		c.wg.Add(1)
		go c.execute(workCh)
	}

	reciever, err := NewReceiver(c.config.AWSConfig)
	if err != nil {
		log.Errf(err, "sqs.commander.run", "err creating new receiver")
		return
	}

	for {
		select {

		case <-c.shutdown:
			return

		default:
			// Connect to Queue and get messages,
			// Receive function blocks for 15 seconds. Please see Receive function in sqs pkg.
			messages, err := reciever.Receive(q)
			if err != nil {
				log.Errf(err, "sqs.commander.run", "error receiving on q[%d]", q)
				// When err occurs it happens instantly. So we sleep for a bit before retrying.
				time.Sleep(time.Second * 5)
				continue
			}

			// if no messages then break.
			if len(messages) == 0 {
				// use for DEBUG log.Infof("sqs.commander.run", "No Messages to process on q[%d]", q)
				continue
			}

			// process messages
			c.processMessages(messages, workCh)
		}
	}
}

// ErrInvalidCommand is when command specified has not been registered.
var ErrInvalidCommand = errors.New("Unknown command in message")

// processMessages processes a slice of messages by sending them via channel
// to waiting routine.
func (c *Commander) processMessages(messages []Message, workCh chan<- doWork) {
	log.Start("sqs.Commander.processMessages")
	// Process Messages
	for _, msg := range messages {
		// Lookup Command

		cmd, ok := c.commands[msg.Command]
		if !ok {
			// response chanel, handles post processing of msg. It will delete
			// message that has no command.
			resp := workResponse{
				msg: msg,
			}
			c.response <- resp

			log.Errf(ErrInvalidCommand, "sqs.Commander.processMessages", "Invalid Command[%s]", msg.Command)
			continue
		}

		// Launch Go Routine
		work := doWork{
			worker: cmd,
			msg:    msg,
		}
		workCh <- work
	}
	log.Complete("sqs.Commander.processMessages")
}

// execute executes a worker command.
func (c *Commander) execute(workCh <-chan doWork) {
	log.Start("sqs.commander.execute")
	defer func() {
		log.Complete("sqs.commander.execute")
		c.wg.Done()
	}()

	extender, err := NewExtender(c.config.AWSConfig)
	if err != nil {
		log.Errf(err, "commander.execute", "error making new extender")
	}

	for {
		select {
		// New work message received.
		case work := <-workCh:
			log.Infof("commander.execute", "exec received, msg id [%s] received", work.msg.MessageID)

			response := workResponse{
				msg: work.msg,
			}

			stopKeepalive := make(chan struct{})
			keepaliveDone := make(chan struct{})
			go func() {
				interval := time.Duration(c.config.VisibilityTimeout-30) * time.Second
				for {
					select {
					case <-time.After(interval):
						log.Infof("commander.execute", "msg id [%s] still processing; restarting timeout", work.msg.MessageID)
						if err := extender.Extend(work.msg.ReceiptHandle, work.msg.Priority, c.config.VisibilityTimeout); err != nil {
							log.Err(err, "commander.execute")
						}
					case <-stopKeepalive:
						keepaliveDone <- struct{}{}
						return
					}
				}
			}()

			err := work.worker.do(work.msg.Payload)
			if err != nil {
				log.Errf(err, "commander.execute", "msg id [%s] generated error", work.msg.MessageID)
				response.err = err
			}

			// Tell keepalive goroutine to stop then wait for it to be done
			stopKeepalive <- struct{}{}
			<-keepaliveDone

			c.response <- response

		case <-c.shutdown:
			return
		}
	}
}

// workResponse is used to return the response from doing work.
type workResponse struct {
	msg Message
	err error
}

// postProcess handles the post processing of the message.
func (c *Commander) postProcess() {
	log.Start("sqs.Command.postProcess")
	defer func() {
		log.Complete("sqs.commander.postProcess")
		// TODO: Handle Panic
		c.wg.Done()
	}()

	receiver, err := NewReceiver(c.config.AWSConfig)
	if err != nil {
		log.Errf(err, "sqs.Command.postProcess", "err creating new sqs.Receiver")
	}

	for {
		select {
		case r := <-c.response:
			log.Infof("sqs.Command.postProcess", "msg id[%s] received for post processing, err[%+v]", r.msg.MessageID, r.err)

			// Remove Message off queue
			if err := receiver.Delete(r.msg.Priority, r.msg.ReceiptHandle); err != nil {
				log.Errf(err, "sqs.Command.postProcess", "msg id[%s]", r.msg.MessageID)
				// TODO: handle error
				// TODO: Alert or Logstash
			}

		case <-c.shutdown:
			return
		}
	}
}

func (c *Commander) shutDown() {
	log.Start("sqs.commander.shutDown")
	// Close Channel to exit the go routines.
	if c.isRunning {
		c.isRunning = false
		close(c.shutdown)
		log.Infof("sqs.commander.shutDown", "waiting for routines to exit...\n")
		c.wg.Wait()
		log.Infof("sqs.commander.shutDown", "all routines exited...\n")
	}

	log.Complete("sqs.commander.shutDown")
}

// Stop stops the commander and closes the stops the goroutines.
func (c *Commander) Stop() {
	c.shutDown()
}
