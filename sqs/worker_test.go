package sqs

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

// TestCommander test the commander can start and be shutdown.
func TestCommander(t *testing.T) {
	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	c.Start()
	time.Sleep(5 * time.Second)
	fmt.Println("stopping")
	c.Stop()
	c.Stop()
	fmt.Println("stopped")
}

// TestBasicWorker tests a basic worker by sending a message and verifying
// it gets processed.
func TestBasicWorker(t *testing.T) {
	ClearQueue(t, HighPriority)

	var output string
	var mu sync.Mutex

	payload := "Hello World"

	f := func(p string) error {
		mu.Lock()
		output = p
		mu.Unlock()

		return nil
	}

	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	c.RegisterCmd("hello", f)
	c.Start()

	s, err := NewSender(c.config.AWSConfig)
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new sender", err)
	}

	m := Message{
		Command:  "hello",
		Priority: HighPriority,
		Payload:  payload,
	}

	_, err = s.Send(&m)
	if err != nil {
		t.Errorf("Exepcted err on send to be nil but was %+v", err)
	}

	time.Sleep(7 * time.Second)

	mu.Lock()
	if output != payload {
		t.Errorf("expected output to be %s, but was %s instead", payload, output)
	}
	mu.Unlock()
	ClearQueue(t, HighPriority)

	c.Stop()
}

// TestBatchMessages test sending batch number of messages to HighPriority
// and LowPriority Queues.
func TestBatchMessages(t *testing.T) {
	// Send 20 Messages to High Priority
	// Send 20 Messages to Low Piriority
	ClearQueue(t, HighPriority)
	ClearQueue(t, LowPriority)

	var mu sync.Mutex
	output := map[string]string{}

	// Worker Function
	f := func(p string) error {

		splits := strings.Split(p, ",")

		mu.Lock()
		output[splits[0]] = splits[1]
		mu.Unlock()

		return nil
	}

	// Create Commander, register cmd and start it.
	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}
	c.RegisterCmd("test_msg", f)
	c.Start()

	// Create High Priority Messages
	var hMsgs []Message
	for i := 0; i < 20; i++ {
		m := Message{
			Command:  "test_msg",
			Priority: HighPriority,
			Payload:  fmt.Sprintf("%d,%d", i, i),
		}
		hMsgs = append(hMsgs, m)
	}

	// Create Low Priority Messages
	var lMsgs []Message
	for i := 20; i < 40; i++ {
		m := Message{
			Command:  "test_msg",
			Priority: LowPriority,
			Payload:  fmt.Sprintf("%d,%d", i, i),
		}
		hMsgs = append(hMsgs, m)
	}

	// Create sender and send message.
	s, err := NewSender(c.config.AWSConfig)
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	result, err := s.SendBatch(hMsgs)
	if err != nil {
		t.Errorf("expected err on send to High Priority to be nil but was %+v", err)
	}

	result2, err := s.SendBatch(lMsgs)
	if err != nil {
		t.Errorf("expected err on send to Low Priority to be nil but was %+v", err)
	}

	for _, r := range result {
		if r.Err != nil {
			t.Errorf("expected msg[%s] in HP Queue to not have error but it did, %+v", r.MessageID, err)
		}
	}

	for _, r := range result2 {
		if r.Err != nil {
			t.Errorf("expected msg[%s] in LP Queue to not have error but it did, %+v", r.MessageID, err)
		}
	}

	time.Sleep(20 * time.Second)

	for i := 0; i < 40; i++ {
		key := fmt.Sprintf("%d", i)
		mu.Lock()
		val, ok := output[key]
		mu.Unlock()

		if !ok {
			t.Errorf("expected key[%s] to exist but it didn't", key)
		}

		if val != key {
			t.Errorf("expected key[%s] to contain value[%s] but it contained [%s]", key, key, val)
		}
	}
	ClearQueue(t, HighPriority)
	ClearQueue(t, LowPriority)

	c.Stop()
}

// TestVolume test sending batch number of messages to HighPriority
// and LowPriority Queues.
func TestVolume(t *testing.T) {
	ClearQueue(t, HighPriority)
	ClearQueue(t, LowPriority)

	type TM struct {
		MId string
		ID  string
	}

	var mu sync.Mutex
	output := map[string]TM{}

	// Worker Function
	f := func(p string) error {
		tmap := map[string]interface{}{}
		if err := json.Unmarshal([]byte(p), &tmap); err != nil {
			return err
		}
		id := tmap["ID"].(string)
		mID := tmap["MId"].(string)

		tm := TM{MId: mID, ID: id}

		mu.Lock()
		output[id] = tm
		mu.Unlock()

		return nil
	}

	// Create Commander, register cmd and start it.
	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}
	c.RegisterCmd("test_msg", f)
	c.Start()

	// Create High Priority Messages
	var hMsgs []Message
	for i := 0; i < 200; i++ {
		tmap := TM{MId: fmt.Sprintf("%d", i), ID: fmt.Sprintf("%d", i)}
		bits, _ := json.Marshal(&tmap)

		m := Message{
			Command:  "test_msg",
			Priority: HighPriority,
			Payload:  string(bits),
		}
		hMsgs = append(hMsgs, m)
	}

	// Create Low Priority Messages
	var lMsgs []Message
	for i := 200; i < 400; i++ {
		tmap := TM{MId: fmt.Sprintf("%d", i), ID: fmt.Sprintf("%d", i)}
		bits, _ := json.Marshal(&tmap)

		m := Message{
			Command:  "test_msg",
			Priority: LowPriority,
			Payload:  string(bits),
		}
		hMsgs = append(hMsgs, m)
	}

	// Create sender and send message.
	s, err := NewSender(c.config.AWSConfig)
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	for idx, msg := range hMsgs {
		_, err := s.Send(&msg)
		if err != nil {
			t.Errorf("expected err on send to High Priority to be nil but was %+v", err)
		}

		if idx%10 == 0 {
			time.Sleep(25 * time.Millisecond)
		}
	}

	for idx, msg := range lMsgs {
		_, err := s.Send(&msg)
		if err != nil {
			t.Errorf("expected err on send to High Priority to be nil but was %+v", err)
		}

		if idx%10 == 0 {
			time.Sleep(25 * time.Millisecond)
		}
	}

	time.Sleep(60 * time.Second)

	for i := 0; i < 4; i++ {
		key := fmt.Sprintf("%d", i)
		mu.Lock()
		val, ok := output[key]
		mu.Unlock()

		if !ok {
			t.Errorf("expected key[%s] to exist but it didn't", key)
		}

		if val.ID != key {
			t.Errorf("expected key[%s] to contain Id value[%s] but it contained [%s]", key, key, val.ID)
		}
		if val.MId != key {
			t.Errorf("expected key[%s] to contain MId value[%s] but it contained [%s]", key, key, val.MId)
		}
	}
	ClearQueue(t, HighPriority)
	ClearQueue(t, LowPriority)

	c.Stop()

}

// TestWorkerPanic test that if a worker function panic it gets handled
// correctly.
func TestWorkerPanic(t *testing.T) {
	ClearQueue(t, HighPriority)

	payload := "Hello World"

	f := func(p string) error {
		panic("Worker Panic happened")
	}

	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	c.RegisterCmd("hello", f)
	c.Start()

	s, err := NewSender(c.config.AWSConfig)
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new sender", err)
	}

	m := Message{
		Command:  "hello",
		Priority: HighPriority,
		Payload:  payload,
	}

	_, err = s.Send(&m)
	if err != nil {
		t.Errorf("Exepcted err on send to be nil but was %+v", err)
	}

	time.Sleep(5 * time.Second)

	ClearQueue(t, HighPriority)
	c.Stop()
}

// TestWorkerError test a worker that returns an error is handled.
func TestWorkerError(t *testing.T) {
	ClearQueue(t, HighPriority)

	payload := "Worker"

	f := func(p string) error {
		return errors.New("Worker Error Occurred.")
	}

	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	c.RegisterCmd("Worker", f)
	c.Start()

	s, err := NewSender(c.config.AWSConfig)
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new sender", err)
	}

	m := Message{
		Command:  "Worker",
		Priority: HighPriority,
		Payload:  payload,
	}

	_, err = s.Send(&m)
	if err != nil {
		t.Errorf("Exepcted err on send to be nil but was %+v", err)
	}

	time.Sleep(5 * time.Second)
	ClearQueue(t, HighPriority)

	c.Stop()
}

func TestSchTask(t *testing.T) {
	ClearQueue(t, HighPriority)

	var runCount int64

	f := func(p string) error {
		atomic.AddInt64(&runCount, 1)
		return nil
	}

	c, err := NewCommander(loadCmdConfig())
	if err != nil {
		t.Fatalf("expected err to be nil but was %+v, creating new commander", err)
	}

	c.RegisterSchTask("tast.sch.task", f, 15)
	c.Start()

	time.Sleep(120 * time.Second)
	if runCount < 7 {
		t.Fatalf("expected run count to be greater than or equal to 7 but was %d instead", runCount)
	}
	ClearQueue(t, HighPriority)
}

func loadCmdConfig() CmdConfig {
	return CmdConfig{
		AWSConfig:   loadConfig(),
		HPWorkers:   10,
		LPWorkers:   2,
		PostProcess: 2,
	}
}
