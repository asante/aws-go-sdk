package sqs

import (
	"encoding/json"
	"errors"
	"net/http"

	"bitbucket.org/asante/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	// LowPriority denotes a message for low priority queue.
	LowPriority = iota // 0

	// HighPriority denotes a message for high priority queue.
	HighPriority // 1
)

type (

	// SQS is the type that implements the mailer interface. It embeds the aws SQS type.
	SQS struct {
		Config *AWSConfig
		*sqs.SQS
	}

	// Sender is the interface used to send messages to SQS.
	Sender interface {
		Send(m *Message) (string, error)
		SendBatch(messages []Message) ([]SendResult, error)
		SendWithDelay(m *Message, delay int64) (string, error)
	}

	// Message is the data type used to encapsulate a message to be sent.
	Message struct {
		MessageID     string
		Command       string
		Priority      int
		Payload       string
		ReceiptHandle string
	}

	// AWSConfig holds the configuration needed for SQS
	AWSConfig struct {
		AWSAccessKey        string
		AWSSecretAccessKey  string
		AWSDefaultRegion    string
		AWSHighPriorityQ    string
		AWSLowPriorityQ     string
		MaxNumberOfMessages int64
		WaitTimeSeconds     int64
		VisibilityTimeout   int64 // No of seconds that msg is locked before re-queue.
	}
)

// ErrInvalidConfig returns an error when the config is missing params.
var ErrInvalidConfig = errors.New("Missing or invalid config paramters")

// Valid determines of the config
func (cfg *AWSConfig) Valid() error {
	if cfg.AWSAccessKey == "" {
		log.Err(ErrInvalidConfig, "context.AWSConfig Invalid AWSAccessKey")
		return ErrInvalidConfig
	}

	if cfg.AWSSecretAccessKey == "" {
		log.Err(ErrInvalidConfig, "context.AWSConfig Invalid AWSSecretAccessKey")
		return ErrInvalidConfig
	}

	if cfg.AWSDefaultRegion == "" {
		log.Err(ErrInvalidConfig, "context.AWSConfig Invalid AWSDefaultRegion")
		return ErrInvalidConfig
	}

	if cfg.AWSHighPriorityQ == "" {
		log.Err(ErrInvalidConfig, "context.AWSConfig Invalid AWSHighPriorityQ")
		return ErrInvalidConfig
	}

	if cfg.AWSLowPriorityQ == "" {
		log.Err(ErrInvalidConfig, "context.AWSConfig Invalid AWSLowPriorityQ")
		return ErrInvalidConfig
	}

	if cfg.MaxNumberOfMessages == 0 {
		cfg.MaxNumberOfMessages = 10
	}

	if cfg.WaitTimeSeconds == 0 {
		cfg.WaitTimeSeconds = 15
	}

	if cfg.VisibilityTimeout == 0 {
		cfg.VisibilityTimeout = 300
	}

	if e := cfg.validateConnection(); e != nil {
		log.Err(e, "context.AWSConfig Unable to validate connection")
		return e
	}

	return nil
}

// validateConnection validates that that the config is valid by getting the
// attributes of the high and low priority queues.
func (cfg *AWSConfig) validateConnection() error {
	log.Start("sqs.validateConnection")
	s := newSQS(cfg)

	attr := "All"
	input := sqs.GetQueueAttributesInput{
		AttributeNames: []*string{&attr},
		QueueUrl:       &cfg.AWSHighPriorityQ,
	}
	// Check HighPriority Queue
	if _, err := s.GetQueueAttributes(&input); err != nil {
		// log
		log.CompleteErr(err, "sqs.validateConnection")
		return err
	}

	// Check LowPriority Queue
	input.QueueUrl = &cfg.AWSLowPriorityQ
	if _, err := s.GetQueueAttributes(&input); err != nil {
		log.CompleteErr(err, "sqs.validateConnection")
		return err
	}

	log.Complete("sqs.validateConnection")
	return nil
}

// NewSender creates a new sender with the provided aws config. Sender is used to send Messages to
// SQS. It returns an error if any of the config fields is blank.
func NewSender(cfg AWSConfig) (Sender, error) {
	if err := cfg.Valid(); err != nil {
		return nil, err
	}
	return newSQS(&cfg), nil
}

// newSQS returns new instance of SQS for an Amazon Config. Uses aws.DefaultConfig if none passed in.
func newSQS(c *AWSConfig) *SQS {
	// Create A new DefaultConfig to be concurrent.
	config := aws.Config{
		Credentials:             credentials.NewStaticCredentials(c.AWSAccessKey, c.AWSSecretAccessKey, ""),
		Endpoint:                aws.String(""),
		Region:                  &c.AWSDefaultRegion,
		DisableSSL:              aws.Bool(false),
		HTTPClient:              http.DefaultClient,
		LogLevel:                aws.LogLevel(0),
		MaxRetries:              aws.Int(aws.UseServiceDefaultRetries),
		DisableParamValidation:  aws.Bool(false),
		DisableComputeChecksums: aws.Bool(false),
		S3ForcePathStyle:        aws.Bool(false),
	}

	s := SQS{
		SQS:    sqs.New(session.New(), &config),
		Config: c,
	}

	return &s
}

// queue returns the queue url based on p which is the priority.
func (s *SQS) queue(p int) string {
	if p == HighPriority {
		return s.Config.AWSHighPriorityQ
	}
	return s.Config.AWSLowPriorityQ
}

// Send is the sender interface method. It sends a message to SQS and returns the message Id
// on success. Otherwise, it returns an error.
func (s *SQS) Send(m *Message) (string, error) {
	return s.SendWithDelay(m, 0)
}

// SendWithDelay sends a message with the specified delay. It sends a message to SQS and
// returns the message Id on success. Otherwise, it returns an error.
func (s *SQS) SendWithDelay(m *Message, delay int64) (string, error) {
	log.Start("sqs.Send")

	// Encode Message to JSON, to be sent as msg body.
	b, err := json.Marshal(m)
	if err != nil {
		log.CompleteErrf(err, "sqs.Send", "error encoding message payload")
		return "", err
	}

	// Specify Queue
	queue := s.queue(m.Priority)

	// Create Input to send to SQS.
	payload := string(b)
	input := sqs.SendMessageInput{
		DelaySeconds: &delay,
		QueueUrl:     &queue,
		MessageBody:  &payload,
	}

	output, err := s.SQS.SendMessage(&input)
	if err != nil {
		log.CompleteErrf(err, "sqs.Send", "error sending to SQS.")
		return "", err
	}

	log.Completef("sqs.Send", "Message Sent[%s]", *output.MessageId)
	return *output.MessageId, nil
}

// SendResult is the result of sending a batch of messages.
type SendResult struct {
	MessageID string
	Command   string
	Err       error
}

// SendBatch sends a batch of messages to SQS. It then returns a result for each one.
func (s *SQS) SendBatch(messages []Message) ([]SendResult, error) {
	log.Start("sqs.SendBatch")

	results := make([]SendResult, len(messages))
	for idx, msg := range messages {

		id, err := s.Send(&msg)
		if err != nil {
			results[idx] = SendResult{
				Command: msg.Command,
				Err:     err,
			}
			continue
		}

		results[idx] = SendResult{
			Command:   msg.Command,
			MessageID: id,
		}
	}

	log.Complete("sqs.SendBatch")
	return results, nil
}

type (
	// Receiver is the interface used to Receive Messages
	Receiver interface {
		Receive(priority int) ([]Message, error)
		Delete(priority int, handle string) error
	}
)

// NewReceiver returns a new Receiver used to receive messages on.
func NewReceiver(cfg AWSConfig) (Receiver, error) {
	if err := cfg.Valid(); err != nil {
		return nil, err
	}
	return newSQS(&cfg), nil
}

// Receive handles the receiving of messages from a queue.
func (s *SQS) Receive(priority int) ([]Message, error) {
	queue := s.queue(priority)

	all := "all"
	input := sqs.ReceiveMessageInput{
		AttributeNames:      []*string{&all},
		QueueUrl:            &queue,
		MaxNumberOfMessages: &s.Config.MaxNumberOfMessages,
		WaitTimeSeconds:     &s.Config.WaitTimeSeconds,
		VisibilityTimeout:   &s.Config.VisibilityTimeout,
	}

	output, err := s.SQS.ReceiveMessage(&input)
	if err != nil {
		log.CompleteErr(err, "sqs.Receive")
		return nil, err
	}

	messages := make([]Message, len(output.Messages))

	for idx, qMsg := range output.Messages {
		var message Message
		log.Infof("sqs.Receive", "MsgID[%s],Body[%s]", *qMsg.MessageId, *qMsg.Body)

		// Assemble message from Queue Message
		if qMsg.Body != nil {
			// main payload.
			if err := json.Unmarshal([]byte(*qMsg.Body), &message); err != nil {
				log.Errf(err, "sqs.Receive", "Error Unmarshalling message body, %+v", *qMsg.Body)
			}
			// receipt handle
			if qMsg.ReceiptHandle != nil {
				message.ReceiptHandle = *qMsg.ReceiptHandle
			}
			// message id
			if qMsg.MessageId != nil {
				message.MessageID = *qMsg.MessageId
			}
			// priority
			message.Priority = priority
		}

		messages[idx] = message
	}

	return messages, nil
}

// Delete delete a message off the queue.
func (s *SQS) Delete(priority int, handle string) error {
	log.Start("sqs.Delete")

	queue := s.queue(priority)

	input := sqs.DeleteMessageInput{
		QueueUrl:      &queue,
		ReceiptHandle: &handle,
	}

	if _, err := s.SQS.DeleteMessage(&input); err != nil {
		log.CompleteErr(err, "sqs.Delete")
		return err
	}

	log.Complete("sqs.Delete")
	return nil
}

type (
	Extender interface {
		Extend(handle string, priority int, timeout int64) error
	}
)

// NewExtender returns a new Extender used to extend message timeouts
func NewExtender(cfg AWSConfig) (Extender, error) {
	if err := cfg.Valid(); err != nil {
		return nil, err
	}
	return newSQS(&cfg), nil
}

// Extend will extend the time allowed to process an inflight message by
// changing it's visibility timeout. The handle of the message is required to
// identify the particular message and the priority value is required to
// identify the message's queue. The timeout value becomes the new message
// visibility value which resets the timer.
func (s *SQS) Extend(handle string, priority int, timeout int64) error {
	log.Start("sqs.Extend")

	queue := s.queue(priority)

	input := sqs.ChangeMessageVisibilityInput{
		QueueUrl:          &queue,
		ReceiptHandle:     &handle,
		VisibilityTimeout: &timeout,
	}
	if _, err := s.SQS.ChangeMessageVisibility(&input); err != nil {
		log.CompleteErr(err, "sqs.Extend")
		return err
	}

	log.Complete("sqs.Extend")
	return nil
}
