package sqs

import (
	"os"
	"testing"
	"time"
)

func TestMessageSend(t *testing.T) {
	ClearQueue(t, HighPriority)

	// Sending
	m := Message{
		Command:  "test_single",
		Priority: HighPriority,
		Payload:  "Hello World",
	}

	s, err := NewSender(loadConfig())
	if err != nil {
		t.Fatalf("Expected err to be nil but was not, %+v", err)
	}

	outputID, err := s.Send(&m)
	if err != nil {
		t.Fatalf("Expected err during send to be nil but was not, %+v", err)
	}

	if outputID == "" {
		t.Error("Expected message id to not be empty but it was")
	}

	time.Sleep(10 * time.Second)

	// Receiving
	r, err := NewReceiver(loadConfig())
	if err != nil {
		t.Fatalf("Expected err to be nil but was not, %+v", err)
	}

	messages, err := r.Receive(HighPriority)
	if err != nil {
		t.Fatalf("Expected err during send to be nil but was not, %+v", err)
	}

	if len(messages) == 0 {
		t.Fatalf("expected messages to contain messages but it didn't")
	}

	var expectMsg Message
	for _, msg := range messages {
		// Enable for Debugging. fmt.Printf("Processing Msg, %+v\n", msg)

		if msg.MessageID == outputID {
			if msg.Command == m.Command {
				expectMsg.Command = msg.Command
				expectMsg.Payload = msg.Payload
				expectMsg.Priority = msg.Priority
			}
		}
		if err := r.Delete(HighPriority, msg.ReceiptHandle); err != nil {
			t.Errorf("Expected err during delete to be nil but was not, %+v", err)
		}
	}

	if expectMsg.Command != m.Command {
		t.Errorf("expected message type, %s to equal, %s but it didn't", expectMsg.Command, m.Command)
	}

	if expectMsg.Payload != m.Payload {
		t.Errorf("expected payload, %+v to equal, %+v but it didn't", expectMsg.Payload, m.Payload)
	}

	if expectMsg.Priority != m.Priority {
		t.Errorf("expected priority, %d to equal, %d but it didn't", expectMsg.Priority, m.Priority)
	}
}

func TestMessageSendBatch(t *testing.T) {
	ClearQueue(t, HighPriority)

	// Sending
	m := Message{
		Command:  "test",
		Priority: HighPriority,
		Payload:  "Hello World",
	}

	m1 := Message{
		Command:  "test_1",
		Priority: HighPriority,
		Payload:  "Hello World_1",
	}

	m2 := Message{
		Command:  "test_3",
		Priority: HighPriority,
		Payload:  "Hello World_3",
	}

	msgs := []Message{m, m1, m2}

	s, err := NewSender(loadConfig())
	if err != nil {
		t.Fatalf("Expected err to be nil but was not, %+v", err)
	}

	results, err := s.SendBatch(msgs)
	if err != nil {
		t.Fatalf("Expected err during send batch to be nil but was not, %+v", err)
	}

	if results == nil {
		t.Error("Expected results to not be nil but they were")
	}

	if len(results) != len(msgs) {
		t.Errorf("Expected results length to equal [%d], instead it is [%d]", len(msgs), len(results))
	}

	for idx, r := range results {
		if r.Command != msgs[idx].Command {
			t.Errorf("Expected Command For Index[%d] to equal [%s] but it was [%s]", idx, msgs[idx].Command, r.Command)
		}
		if r.MessageID == "" {
			t.Errorf("Expected MessageID For Index[%d] to not be empty", idx)
		}
		if r.Err != nil {
			t.Errorf("Expected Error For Index[%d] to be empty, instead it was %+v", idx, r.Err)
		}
	}

	time.Sleep(10 * time.Second)

	// Receiving

	r, err := NewReceiver(loadConfig())
	if err != nil {
		t.Fatalf("Expected err to be nil but was not, %+v", err)
	}

	messages, err := r.Receive(HighPriority)
	if err != nil {
		t.Fatalf("Expected err during send to be nil but was not, %+v", err)
	}

	// Keep receiving until we have all the messages.
	var counter int
	for {
		time.Sleep(1 * time.Second)
		if len(messages) == len(msgs) {
			break
		}
		if counter == 15 {
			break
		}
		m, err := r.Receive(HighPriority)
		if err != nil {
			t.Fatalf("Expected err during send to be nil but was not, %+v", err)
		}
		messages = append(messages, m...)
		counter++
	}

	if len(messages) == 0 {
		t.Fatalf("expected messages to contain messages but it didn't")
	}

	if len(messages) != len(msgs) {
		t.Fatalf("expected messages to contain [%d] messages but it didn't", len(msgs))
	}

	var foundCounter int
	for _, msg := range messages {
		// Enable for Debugging. fmt.Printf("Processing Msg, %+v\n", msg)
		// loop through sent messages and assert.
		var found bool

		for _, emsg := range msgs {

			if emsg.Command == msg.Command {

				if msg.Payload != emsg.Payload {
					t.Errorf("expected payload, %+v to equal, %+v but it didn't", msg.Payload, emsg.Payload)
				}

				if msg.Priority != emsg.Priority {
					t.Errorf("expected priority, %d to equal, %d but it didn't", msg.Priority, emsg.Priority)
				}

				found = true
				foundCounter++
				break
			}
		}

		if !found {
			t.Errorf("Expected to find msg with command, %s, but it was not found", msg.Command)
		}

		if err := r.Delete(HighPriority, msg.ReceiptHandle); err != nil {
			t.Errorf("Expected err during delete to be nil but was not, %+v", err)
		}
	}

	if foundCounter != len(msgs) {
		t.Fatalf("expected to find [%d] messages but found [%d] instead", len(msgs), foundCounter)
	}
}

func loadConfig() AWSConfig {
	return AWSConfig{
		AWSAccessKey:        os.Getenv("AWS_ACCESS_KEY_ID"),
		AWSSecretAccessKey:  os.Getenv("AWS_SECRET_ACCESS_KEY"),
		AWSDefaultRegion:    os.Getenv("AWS_DEFAULT_REGION"),
		AWSHighPriorityQ:    os.Getenv("AWS_HIGH_PRIORITY_QUEUE"),
		AWSLowPriorityQ:     os.Getenv("AWS_LOW_PRIORITY_QUEUE"),
		MaxNumberOfMessages: 10,
		WaitTimeSeconds:     15,
	}
}

func ClearQueue(t *testing.T, priority int) {
	r, err := NewReceiver(loadConfig())
	if err != nil {
		t.Fatalf("err loading new receiver, %+v", err)
		return
	}

	var counter int
	for {
		messages, _ := r.Receive(priority)
		for _, m := range messages {
			r.Delete(priority, m.ReceiptHandle)
		}
		if len(messages) == 0 {
			return
		}
		if counter == 15 {
			return
		}
		counter++
		time.Sleep(1 * time.Second)
	}
}

func TestClear(t *testing.T) {
	ClearQueue(t, HighPriority)
	ClearQueue(t, LowPriority)
}

func TestValidateConnect(t *testing.T) {
	cfg := loadConfig()

	if e := cfg.validateConnection(); e != nil {
		t.Fatalf("expected to validate connection but unable to, %+v", e)
	}

	hq := cfg.AWSHighPriorityQ
	cfg.AWSHighPriorityQ = "foo"
	if e := cfg.validateConnection(); e == nil {
		t.Error("expected to validate connection to return error but it did not")
	}
	cfg.AWSHighPriorityQ = hq

	dr := cfg.AWSDefaultRegion
	cfg.AWSDefaultRegion = ""
	if e := cfg.validateConnection(); e == nil {
		t.Error("expected to validate connection to return error but it did not")
	}
	cfg.AWSDefaultRegion = dr

	cfg.AWSAccessKey = "fjkdfjjjjfjkhs"
	if e := cfg.validateConnection(); e == nil {
		t.Error("expected to validate connection to return error but it did not")
	}
}
