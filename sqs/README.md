### SQS Package
---

    SQS is a wrapper around Amazon's SQS Go API (github.com/aws/aws-sdk-go). It provides the following functionality:

    * Send Message to Queue
    * Receive Message from Queue
    * Delete Message from Queue
    
#### Config

To create a receiver or sender an instance of `AWSConfig` type is required. If any of the fields is empty or invalid an error
will be returned when creating a receiver or sender.

    cfg:= AWSConfig {
        AWSAccessKey:        "", // Required
        AWSSecretAccessKey:  "", // Required
        AWSDefaultRegion:    "", // Required
        AWSHighPriorityQ:    "", // Required
        AWSLowPriorityQ:     "", // Required
        MaxNumberOfMessages: 10, // Not Required, defaults to 10
        WaitTimeSeconds:     15, // Not Required, defaults to 15
    }


#### Sending Messages

The SQS package gives the ability to send single messages or a batch of messages.

    // Create configuration 
    cfg:= AWSConfig {
        AWSAccessKey:        "", // Required
        AWSSecretAccessKey:  "", // Required
        AWSDefaultRegion:    "", // Required
        AWSHighPriorityQ:    "", // Required
        AWSLowPriorityQ:     "", // Required
        MaxNumberOfMessages: 10, // Not Required, defaults to 10
        WaitTimeSeconds:     15, // Not Required, defaults to 15
    }

    // Create A new Sender
    s, err := NewSender(cfg)
    if err != nil {
        // Handle Err
    }

    // Create a Message
    m := Message{
        Command:  "test_single",    // Command defines the command to run.
        Priority: HighPriority,     // Priority determines the priority, HighPriority or LowPriority.
        Payload:  "Hello World",    // Payload is the data sent to the command. 
    }

    // Call the Send method on Sender
    messageID, err := s.Send(&m)
    if err != nil {
        // Handle Error
    }

    messageID will contain the message id of message sent to SQS.

Sending Batch messages is similiar to sending a single message
    
    // Create configuration 
    cfg:= AWSConfig {
        AWSAccessKey:        "", // Required
        AWSSecretAccessKey:  "", // Required
        AWSDefaultRegion:    "", // Required
        AWSHighPriorityQ:    "", // Required
        AWSLowPriorityQ:     "", // Required
        MaxNumberOfMessages: 10, // Not Required, defaults to 10
        WaitTimeSeconds:     15, // Not Required, defaults to 15
    }

    // Create a new Sender
    s, err := NewSender(cfg)
    if err != nil {
        // Handle Err
    }

    // Create Messages to send
    m := Message{
        Command:  "test",
        Priority: HighPriority,
        Payload:  "Hello World",
    }

    m1 := Message{
        Command:  "test_1",
        Priority: HighPriority,
        Payload:  "Hello World_1",
    }

    m2 := Message{
        Command:  "test_2",
        Priority: HighPriority,
        Payload:  "Hello World_3",
    }

    msgs := []Message{m, m1, m2}
  
    // Send Batch of messages
    results, err := s.SendBatch(msgs)
    if err != nil {
        // Handler Error
    }

The output from the SendBatch method is a slice of `SendResult` type as listed below. The slice
will contain an element for each message sent on the batch.

    type SendResult struct {
        MessageID string    // Message Id of the sent message
        Command   string    // Command of the message sent.
        Err       Error     // Error denotes if there was any error.
    }


#### Receiving Messages

The SQS package allows the receiving of messages from SQS Queues. 

Create New Receiver.
 
    // Create configuration 
    cfg:= AWSConfig {
        AWSAccessKey:        "", // Required
        AWSSecretAccessKey:  "", // Required
        AWSDefaultRegion:    "", // Required
        AWSHighPriorityQ:    "", // Required
        AWSLowPriorityQ:     "", // Required
        MaxNumberOfMessages: 10, // Not Required, defaults to 10
        WaitTimeSeconds:     15, // Not Required, defaults to 15
    }

    r, err := NewReceiver(cfg)
    if err != nil {
        // Handle Err
    }
    
Receive method is called to receive messages. It takes HighPriority or LowPriority
as its parameter.

    messages, err := r.Receive(HighPriority)
    if err != nil {
        // Handle Error
    }

messages will contain a slice of messages. The ReceiptHandle field on the message
will be populated with the id needed to delete the message off the queue.

#### Deleting Messages

Deletes a message off the Queue. It requires the receipt handle of the message and the
priority queue.

    // Create configuration 
    cfg:= AWSConfig {
        AWSAccessKey:        "", // Required
        AWSSecretAccessKey:  "", // Required
        AWSDefaultRegion:    "", // Required
        AWSHighPriorityQ:    "", // Required
        AWSLowPriorityQ:     "", // Required
        MaxNumberOfMessages: 10, // Not Required, defaults to 10
        WaitTimeSeconds:     15, // Not Required, defaults to 15
    }

    // New Receiver
    r, err := NewReceiver(cfg)
    if err != nil {
        // Handle Err
    }
    
    // Delete Message 
    err := r.Delete(HighPriority, m.ReceiptHandle)
    if err != nil {
        // Handle Error
    }    


#### Running Workers 

The consumption of message of the queues is handled by the `Commander` type. It allows the
registering of commands which is defined as `type WorkFunc func(payload interface{}) error`



Create a commander instance. To create a commander an instance of `CmdConfig` type is required.
The `CmdConfig` type embeds a `AWSConfig` type and adds additional fields related to worker configuration.
    
    // Create configuration 
    cfg:= CmdConfig {
            AWSConfig: AWSConfig {
                            AWSAccessKey:        "", // Required
                            AWSSecretAccessKey:  "", // Required
                            AWSDefaultRegion:    "", // Required
                            AWSHighPriorityQ:    "", // Required
                            AWSLowPriorityQ:     "", // Required
                            MaxNumberOfMessages: 10, // Not Required, defaults to 10
                            WaitTimeSeconds:     15, // Not Required, defaults to 15
                        },
            HPWorkers:   10, // High Priority Queue, no of workers. Defaults to 10
            LPWorkers:   2,  // Low Priority Queue, no of workers. Defaults to 2
            PostProcess: 2,  // Post Process no of workers. Defaults to 2
        }

    c, err := NewCommander(cfg)
    if err != nil {
        // Handle Err
    }

Register commands via `RegisterCmd(cmd string, f WorkFunc)` When a message arrives with a
specific command the registered `WorkFunc` will be called for that command. 

    f:= func(payload interface{}) error {
        return nil
    }

    c.RegisterCmd("cmd", f)

Start the commander instance. It will then start to process HighPriority and LowPriority
messages as they come in.

    c.Start()


Stop the commander instace when complete or shutting down.

    c.Stop()