package main

import (
	"flag"
	"os"

	"bitbucket.org/asante/log"
	"bitbucket.org/wbm/aws-go-sdk/sns"
)

func main() {
	log.Init("[publisher]")

	topic := os.Getenv("TOPIC_ARN")
	if topic == "" {
		log.Info("main", "Missing Topic ARN")
		os.Exit(1)
	}

	var msg string
	flag.StringVar(&msg, "m", "", "Message to publish")
	flag.Parse()

	if msg == "" {
		log.Info("main", "Message Cannot be blank.")
		os.Exit(1)
	}

	s := sns.NewPublisher()
	id, err := s.Publish(topic, msg, "Test Subject")
	if err != nil {
		log.Errf(err, "main", "Err publishing message to topic[%s]", topic)
		return
	}

	log.Infof("main", "Message published successfully, id[%s]", id)

	p := struct {
		CommentID string
		Comment   string
	}{"1222", msg}

	m := sns.Message{
		UserID:  "123-456-789",
		Subject: "Message Subject Here",
		Payload: p,
	}

	id, err = s.PublishMessage(topic, &m)
	if err != nil {
		log.Errf(err, "main", "Err publishing message to topic[%s]", topic)
		return
	}

	log.Infof("main", "Message published successfully, id[%s]", id)
}
