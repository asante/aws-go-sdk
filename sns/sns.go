package sns

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"sync"

	"bytes"

	"bitbucket.org/asante/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

type (
	// snsMgr manages the sns subscriptions of this process.
	snsMgr struct {
		topics map[string]string
		sync.RWMutex
	}
)

// Topic Manager

var mgr *snsMgr

func init() {
	mgr = &snsMgr{topics: map[string]string{}}
}

// AddTopic adds a topic the internal list of topics.
func (s *snsMgr) AddTopic(topic, subscription string) {
	s.Lock()
	{
		s.topics[topic] = subscription
	}
	s.Unlock()
}

// Subscription returns the subscription ARN for a particular topic.
func (s *snsMgr) Subscription(topic string) string {
	s.RLock()
	defer mgr.RUnlock()
	if sub, ok := s.topics[topic]; ok {
		return sub
	}
	return ""
}

// RemoveTopic removes a topic the internal list of topics.
func (s *snsMgr) RemoveTopic(topic string) {
	s.Lock()
	{
		delete(s.topics, topic)
	}
	s.Unlock()
}

// Subscriber

type (
	// SNS is the main type exposed to use the SNS service
	SNS struct {
		*sns.SNS
	}

	// Subscriber is the interface to allow subscribtion to a topic.
	Subscriber interface {
		Subscribe(topic string, endpoint string) error
		Confirm(topic, token string) error
		Unsubscribe(subscription string) error
	}
)

// ErrInvalidEndpoint is when an invalid endpoint has been sent.
var ErrInvalidEndpoint = errors.New("Invalid Endpoint")

// newSNS return a new SNS instance.
func newSNS(c ...*aws.Config) *SNS {
	if len(c) == 0 {
		c = append(c, defaults.Config())
		c[0].Region = aws.String(os.Getenv("AWS_DEFAULT_REGION"))
	}

	return &SNS{sns.New(session.New(), c[0])}
}

// NewSubscriber returns a new Subscriber instance using the AWS config sent.
func NewSubscriber(c ...*aws.Config) Subscriber {
	return newSNS(c...)
}

// Subscribe subscribes a to a topic which will the post a message to the returned endpoint.
// It returns the ARN of the topic subscription or an error.
func (s *SNS) Subscribe(topic string, endpoint string) error {
	log.Startf("sns.Subscribe", "topic[%s], endpoint[%s]", topic, endpoint)
	// Read Config from environment.

	// Read Protocol from String
	var protocol []string
	if protocol = strings.Split(endpoint, ":"); len(protocol) == 0 {
		log.CompleteErr(ErrInvalidEndpoint, "Subscribe")
		return ErrInvalidEndpoint
	}

	//Create Input and Subscribe.
	input := sns.SubscribeInput{
		Endpoint: &endpoint,
		Protocol: &protocol[0],
		TopicArn: &topic,
	}

	subOut, err := s.SNS.Subscribe(&input)
	if err != nil {
		log.CompleteErr(err, "sns.Subscribe Err from AWS")
		return err
	}

	log.Infof("sns.Subscribe", "subscription output[%+v]", subOut)

	mgr.AddTopic(topic, *subOut.SubscriptionArn)

	log.Complete("sns.Subscribe")
	return nil
}

// Confirm confirms a subscription token.
func (s *SNS) Confirm(topic, token string) error {
	log.Startf("sns.Subscribe", "token length[%d]", len(token))

	var authOnUnsubscribe = "false"

	// Confirm Subscription.
	confInput := sns.ConfirmSubscriptionInput{
		AuthenticateOnUnsubscribe: &authOnUnsubscribe,
		Token:                     &token,
		TopicArn:                  &topic,
	}

	confOut, err := s.SNS.ConfirmSubscription(&confInput)
	if err != nil {
		log.CompleteErr(err, "Confirm Subscription Err from AWS")
		return err
	}

	mgr.AddTopic(topic, *confOut.SubscriptionArn)
	log.Complete("sns.Subscribe")
	return nil
}

// Unsubscribe unsubscribes from a topic.
func (s *SNS) Unsubscribe(topic string) error {
	log.Start("sns.Unsubscribe")

	subscrip := mgr.Subscription(topic)
	if subscrip == "" {
		return errors.New("No Subscriptions for topic, %s")
	}

	input := sns.UnsubscribeInput{SubscriptionArn: &subscrip}

	if _, err := s.SNS.Unsubscribe(&input); err != nil {
		log.CompleteErrf(err, "sns.Unbscribe Err from AWS", "topic[%s]", subscrip)
		return err
	}

	mgr.RemoveTopic(topic)

	log.Complete("sns.Unsubscribe")
	return nil
}

// Publisher

type (
	// Publisher is the interface that allows publishing to a topic.
	Publisher interface {
		Publish(topic string, message interface{}, subject string) (string, error)
		PublishMessage(topic string, m *Message) (string, error)
	}

	// sendPayload is an internal type used to wrap AMZ message, that requires a
	// default field.
	sendPayload struct {
		Default string
		Message *Message
	}

	// Message represents the a message that is to be delivered to out through
	// the WS server.
	Message struct {
		ID      string
		UserID  string
		Subject string
		Payload interface{}
	}
)

// String implements the fmt.Stringer interface.
func (m *Message) String() string {
	bytes, _ := json.Marshal(&m)
	return string(bytes)
}

// ErrEncodingMsg is an error when encoding a message to be published.
var ErrEncodingMsg = errors.New("Error encoding message payload")

// NewPublisher returns a new publisher instance to be able to publish to a topic.
func NewPublisher(c ...*aws.Config) Publisher {
	return newSNS(c...)
}

// Publish publishes a message to a topic based on the arn. Subject has a max length of
// 100 characters. Subject cannot contain line breaks.
func (s *SNS) Publish(topic string, message interface{}, subject string) (string, error) {
	m := Message{Payload: message, Subject: subject}
	return s.PublishMessage(topic, &m)
}

// PublishMessage publishes a message type to a topic based on the arn. Subject has a max length of
// 100 characters. Subject cannot contain line breaks.
func (s *SNS) PublishMessage(topic string, m *Message) (string, error) {
	log.Startf("sns.Publish", "topic[%s]", topic)

	// Wrap message in send payload and marshal to json.
	p := sendPayload{Default: "", Message: m}

	bytes, err := json.Marshal(&p)
	if err != nil {
		log.CompleteErrf(err, "sns.Publish", "err marshalling payload, topic[%s], msg[%+v]", topic, m)
		return "", ErrEncodingMsg
	}

	body := string(bytes)
	r := "raw"

	// publish message.
	input := sns.PublishInput{
		Message:          &body,
		Subject:          &m.Subject,
		TopicArn:         &topic,
		MessageStructure: &r,
	}

	log.Infof("sns.Publish", "input [%+v]", input)

	output, err := s.SNS.Publish(&input)
	if err != nil {
		log.CompleteErrf(err, "sns.Publish", " error publishing topic[%s], msg[%+v], subject[%s]", topic, m, m.Subject)
		return "", err
	}

	log.Completef("sns.Publish", "topic[%s]", topic)
	return *output.MessageId, nil
}

// messageIn is the type that holds the data for an incoming message from AMZ Sns.
type messageIn struct {
	Type             string `json:"Type"`
	Messageid        string `json:"MessageId"`
	Token            string `json:"Token"`
	Topicarn         string `json:"TopicArn"`
	Subject          string `json:"Subject"`
	Message          string `json:"Message"`
	SubscribeURL     string `json:"SubscribeURL"`
	Timestamp        string `json:"Timestamp"`
	Signatureversion string `json:"SignatureVersion"`
	Signature        string `json:"Signature"`
	Signingcerturl   string `json:"SigningCertURL"`
	Unsubscribeurl   string `json:"UnsubscribeURL"`
}

// Handler

// Handler returns an http handler from a function that takes a fmt.String and returns an error. This is to be used
// as an endpoint for an Incoming message. If message is successfuly received and decoded, then the function fm will be called.
func Handler(fm func(m fmt.Stringer) error) func(w http.ResponseWriter, r *http.Request) {

	f := func(w http.ResponseWriter, r *http.Request) {
		log.Start("sns.Handler")

		// Recieve Response From AMZ & Decode
		var mIn messageIn

		var buffer bytes.Buffer
		reader := io.TeeReader(r.Body, &buffer)

		if err := json.NewDecoder(reader).Decode(&mIn); err != nil {
			log.CompleteErrf(err, "sns.Hanlder", "Err decoding message")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Infof("sns.Handler", "POST Body: \n %s \n", buffer.Bytes())
		log.Infof("sns.Handler", "Processing Message ID[%s], Type[%s]", mIn.Messageid, mIn.Type)

		// Handle the Subscription Confirmation to a topic.
		if mIn.Type == "SubscriptionConfirmation" {
			s := NewSubscriber()
			if err := s.Confirm(mIn.Topicarn, mIn.Token); err != nil {
				log.Errf(err, "sns.Handler", "error confirming subscription")
				http.Error(w, "error confirming subscription: "+err.Error(), http.StatusInternalServerError)
			}
			log.Complete("sns.Handler - subscription confirmation")
			w.WriteHeader(http.StatusOK)
			return
		}

		// Type == Notification

		// Load sns message
		message, err := newMessage(&mIn)
		if err != nil {
			log.Errf(err, "sns.Handler", "error unmarshaling message")
			http.Error(w, "error unmarshaling message: "+err.Error(), http.StatusInternalServerError)

			return
		}

		if err := fm(message); err != nil {
			log.CompleteErrf(err, "sns.Hanlder", "Err processing message")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		log.Complete("sns.Handler")
		w.WriteHeader(http.StatusOK)
	}
	return f
}

// newMessage creates a new message from an amazon message.
func newMessage(mIn *messageIn) (*Message, error) {
	if mIn.Message == "" {
		return &Message{}, nil
	}

	var p sendPayload
	err := json.Unmarshal([]byte(mIn.Message), &p)
	if err != nil {
		return &Message{
			ID:      mIn.Messageid,
			Subject: mIn.Subject,
			Payload: mIn.Message,
		}, nil

	}
	// No Message sent
	if p.Message == nil {
		return &Message{}, nil
	}

	// Set Id and return
	p.Message.ID = mIn.Messageid
	return p.Message, nil
}
