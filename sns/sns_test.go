package sns

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"
)

const msg = `{
  "Type": "Notification",
  "MessageId": "a49c4151-c015-59b8-a648-0cb35d3cab8f",
  "TopicArn": "arn:aws:sns:us-west-2:591169026732:ayvws-dev",
  "Subject": "Test Subject",
  "Message": "Hello World",
  "Timestamp": "2015-05-29T16:59:21.774Z",
  "SignatureVersion": "1",
  "Signature": "sS3E58QkUPPFwdNPlwPq5MtoBdjlQUFbB4b2N6b9o6sAHAUbfjcH1ml2c8Og+3SQzW43oVPtdKG8mOLSQto9aKbFZwToE5eJHxwiFeI8DrY+aHosFuKoUWBdjFITq4VwIqZNKmfAg5OIjQWsIeWuCynnjuRAlnCQqOuxOeRKkTQEb2VLQ40ARp1IchN3ju2fmLNvyBZYCbuhZMxGkw0JJviKWUoYdcPtmCRCIKT56RZrACo94w8viUYuhJ6VBWAxrO/csrMVhmTTlt5IPWi1BSA9C+vGVSwdnyH9wMSXVdyL15QcFFMgPyYTc0YB7Nq59MBp+dZKnpyPZr0auTlVTQ==",
  "SigningCertURL": "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",
  "UnsubscribeURL": "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:591169026732:ayvws-dev:ec6b56c5-f2d6-4638-8604-5ed3f9be695f",
  "MessageAttributes": {
    "AWS.SNS.MOBILE.MPNS.Type": {
      "Type": "String",
      "Value": "token"
    },
    "AWS.SNS.MOBILE.WNS.Type": {
      "Type": "String",
      "Value": "wns/badge"
    },
    "AWS.SNS.MOBILE.MPNS.NotificationClass": {
      "Type": "String",
      "Value": "realtime"
    }
  }
}`

func TestUnmarshalMessage(t *testing.T) {
	var mIn messageIn
	err := json.Unmarshal([]byte(msg), &mIn)
	if err != nil {
		t.Fatalf("Error unmarshaling message, %+v", err)
	}

	if mIn.Message == "" {
		t.Error("Message not unmarshalled")
	}

	if mIn.Type == "" {
		t.Error("Type not unmarshalled")
	}
	if mIn.Subject == "" {
		t.Error("Subject not unmarshalled")
	}

	if mIn.Topicarn == "" {
		t.Error("Topicarn not unmarshalled")
	}
}

// TestPublishMessage test the subscribe, confirm, publish message and unsubscribe in 1 test.
// Currently it requires NGROK to be in the path.
func TestPublishMessage(t *testing.T) {
	topic := os.Getenv("TOPIC_ARN")
	// create channels
	// 1. subscribe channel, publish channel
	subCh := make(chan error)
	pubCh := make(chan error)

	msg := "TestPublishMessage"

	// Start Server
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mtype := r.Header.Get("x-amz-sns-message-type")
		log.Printf("TestServer request received, type=> %s", mtype)

		f := func(fm fmt.Stringer) error {
			log.Println(fm)

			s := fm.String()

			var m Message
			err := json.Unmarshal([]byte(s), &m)
			if err != nil {
				t.Errorf("Err unmarshalling msg, Msg[%s], Err[%+v]", s, err)
				return err
			}
			if m.Payload != msg {
				t.Errorf("Expected Msg[%s], Got[%s]", msg, m.Payload)
				e := errors.New("Did not receive message")
				return e
			}

			return nil
		}
		h := Handler(f)

		recorder := httptest.NewRecorder()
		h(recorder, r)

		if mtype == "SubscriptionConfirmation" {
			if recorder.Code != 200 {
				log.Printf("Error Processing Message.")
				w.WriteHeader(http.StatusInternalServerError)

				subCh <- errors.New("Error Processing subscription Message.")
				return
			}
			subCh <- nil
			return
		}

		if mtype == "Notification" {
			if recorder.Code != 200 {
				log.Printf("Error Processing Published Message.")
				w.WriteHeader(http.StatusInternalServerError)

				// else it will be a notification
				pubCh <- errors.New("Error Processing Published Message.")
				return
			}
			log.Println("Successful Response, test server, sending on nil on CH.")
			pubCh <- nil
			return
		}

		log.Println("Unknown Message Received")
	}))
	defer server.Close()

	// Start NGROK
	scheme := strings.Split(server.URL, ":")
	if len(scheme) != 3 {
		t.Fatalf("Invalid Url from test server, no port, %s", server.URL)
	}

	log.Printf("Port=> %s\n", scheme[2])
	cmd := exec.Command("ngrok", []string{"http", scheme[2]}...)
	if err := cmd.Start(); err != nil {
		t.Fatalf("Unable to Start NGROK, %+v", err)
	}
	defer cmd.Process.Kill()

	time.Sleep(5 * time.Second)

	log.Printf("PID: %d", cmd.Process.Pid)
	// Get NGROK URL
	nURL := ngrokURL(t)
	if nURL == "" {
		t.Fatal("NGROK Url is empty")
	}
	nURL += "/in"

	log.Printf("Endpoint: %s\n", nURL)

	// Unsubscribe.
	s := NewSubscriber()
	if err := s.Subscribe(topic, nURL); err != nil {
		t.Fatalf("Error subscribing to topic[%s], Err: %+v", topic, err)
	}

	for {
		select {
		case <-time.After(120 * time.Second):
			t.Error("Test Timeout ....")
			return
		case e := <-subCh:
			log.Printf("received on subscribe channel, %+v\n", e)
			if e != nil {
				t.Errorf("Error on subscribe channel, %+v", e)
				return
			}

			// Successfully Subscribed now test publishing.
			p := NewPublisher()
			_, err := p.Publish(topic, msg, "TestSubject")
			if err != nil {
				t.Errorf("Error publishing to topic[%s], %+v", topic, err)
				if err := s.Unsubscribe(topic); err != nil {
					t.Fatalf("Error unsubscribing from topic[%s], Err: %+v", topic, err)
				}
				log.Println("Exiting....")
				return
			}
			log.Println("Message Published success...")
			time.Sleep(2 * time.Second)

		case err := <-pubCh:
			if err != nil {
				t.Errorf("Received on publish channel, %+v", err)
			}

			if err := s.Unsubscribe(topic); err != nil {
				t.Fatalf("Error unsubscribing from topic[%s], Err: %+v", topic, err)
			}
			log.Println("Exiting....")
			return
		}
	}
}

// TestSubscribeAndConfirm test subscribing and confirming a subscription. If successful
// it will unsubscribe from the topic. It requires ngrok (https://ngrok.com/) and the ngrok
// binary to be in the path.
func TestSuscribeAndConfirm(t *testing.T) {

	topic := os.Getenv("TOPIC_ARN")
	// create channels
	// 1. action channel, close channel
	stCh := make(chan error)
	//clCh := make(chan error)

	// Start Server
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//mtype := r.Header.Get("x-amz-sns-message-type")
		f := func(fm fmt.Stringer) error {
			log.Println(fm)
			return nil
		}
		h := Handler(f)

		recorder := httptest.NewRecorder()
		h(recorder, r)
		if recorder.Code != 200 {
			log.Printf("Error Processing Message.")
			w.WriteHeader(http.StatusInternalServerError)
			stCh <- errors.New("Error Processing Message.")
		}

		log.Println("Successful Response, test server, sending on nil on CH.")
		stCh <- nil

	}))
	defer server.Close()

	// Start NGROK
	scheme := strings.Split(server.URL, ":")
	if len(scheme) != 3 {
		t.Fatalf("Invalid Url from test server, no port, %s", server.URL)
	}

	log.Printf("Port=> %s\n", scheme[2])
	cmd := exec.Command("ngrok", []string{"http", scheme[2]}...)
	if err := cmd.Start(); err != nil {
		t.Fatalf("Unable to Start NGROK, %+v", err)
	}
	defer cmd.Process.Kill()

	time.Sleep(5 * time.Second)

	log.Printf("PID: %d", cmd.Process.Pid)
	// Get NGROK URL
	nURL := ngrokURL(t)
	if nURL == "" {
		t.Fatal("NGROK Url is empty")
	}
	nURL += "/in"

	log.Printf("Endpoint: %s\n", nURL)

	// Unsubscribe.
	s := NewSubscriber()
	if err := s.Subscribe(topic, nURL); err != nil {
		t.Fatalf("Error subscribing to topic[%s], Err: %+v", topic, err)
	}

	select {
	case <-time.After(60 * time.Second):
		t.Error("Test Timeout ....")
		return

	case e := <-stCh:
		log.Printf("received on start channel, %+v\n", e)
		if e != nil {
			t.Errorf("Error on start channel, %+v", e)
			return
		}

		if err := s.Unsubscribe(topic); err != nil {
			t.Fatalf("Error unsubscribing from topic[%s], Err: %+v", topic, err)
		}
		log.Println("Exiting....")

		return
	}
}

func ngrokURL(t *testing.T) string {
	var nTunnels struct {
		Tunnels []struct {
			Name      string `json:"name"`
			URI       string `json:"uri"`
			PublicURL string `json:"public_url"`
			Proto     string `json:"proto"`
		} `json:"tunnels"`
		URI string `json:"uri"`
	}

	resp, err := http.Get("http://localhost:4040/api/tunnels")
	if err != nil {
		t.Fatalf("Unable to get NGROK Url, %+v", err)
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&nTunnels); err != nil {
		t.Fatal("Unable to get NGROK Url, unmarshaling tunnel list")
	}

	if len(nTunnels.Tunnels) == 0 {
		t.Fatal("Unable to get NGROK Url, no active tunnels")
	}

	return nTunnels.Tunnels[0].PublicURL
}
