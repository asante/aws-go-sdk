### SNS Package
---

    SNS is a wrapper around Amazon's SNS Go API (github.com/awss/aws-sdk-go). It provides the following functionality:

    * Subscribe to a Topic
    * Confirm the subscription to a Topic
    * Unsubscribe from a Topic

    * Publish a Message to a Topic

    The following Environment Variables should be present in your environment:

    * AWS_ACCESS_KEY_ID
    * AWS_SECRET_ACCESS_KEY
    * AWS_DEFAULT_REGION


#### Subscriber
---

Subscriber is the interface that exposes functionality related to subscribing to a topic.

    // Subscriber is the interface to allow subscription to a topic.
    Subscriber interface {
        Subscribe(topic string, endpoint string) error
        Confirm(topic, token string) error
        Unsubscribe(subscription string) error
    }

    // Create new subscriber, pulls credentials from environment
    s := sns.NewSubscriber()

    // Create a new subscriber, passing in credentials of type aws.Config
    // Which is In **github.com/awslabs/aws-sdk-go/aws** package.
    var cfg aws.Config
    s := sns.NewSubscriber(&cfg)

##### Subscribe

Subscribe will subscribe to a Topic with an endpoint. The Amazon SNS Service will post a subscription confirmation message to the endpoint.

    s := sns.NewSubscriber()
	if err := s.Subscribe(topic, endpt); err != nil {
	    // Handle Error
    }


##### Confirm Subscription

Confirm subscription will confirm a subscription.  

    s := NewSubscriber()
    if err := s.Confirm(topicARN, Token); err != nil {
      // log error
    }

##### Unsubscribe Subscription

Unsubscribes an endpoint from a subscription

    s := sns.NewSubscriber()
	if err := s.Unsubscribe(topicARN); err != nil {
      // log error
    }

##### Handler

Handler allows you to add the http.Handler below as your endpoint for a Topic. It will handle the subscription confirmation automatically. It will also parse an incoming message and pass it to your function.

    // Handler takes a function that takes a fmt.Stringer and returns an error.
    // It returns an http.HanlderFunc that can be used as your endpoint for a topic.
    Handler(fm func(m fmt.Stringer) error) func(w http.ResponseWriter, r *http.Request)


#### Publisher
---

Publisher is the interface that exposes functionality related to publishing a message to a topic.

    // Publisher is the interface that allows publishing to a topic.
    Publisher interface {

        // Publish publishes any value to the Topic ARN and with the passed in subject.
        Publish(topicARN string, message interface{}, subject string) (string, error)

        // Publish publishes a Message to the Topic ARN. Message is defined below.
        PublishMessage(topic string, m *Message) (string, error)
    }

    // Message represents the a message that is to be delivered via a Topic.
	  Message struct {
            // ID gets Assigned the AMZ SNS Id on the recieving end.
		    ID      string
		    UserID  string
		    Subject string
		    Payload interface{}
	  }

    // Create new Publisher, pulls credentials from environment
    p := sns.NewPublisher()

    // Create a new publisher, passing in credentials of type aws.Config
    // Which is In **github.com/awslabs/aws-sdk-go/aws** package.
    var cfg aws.Config
    p := sns.NewPublisher(&cfg)



##### Publish

Publish publishes any value to the Topic ARN and with the passed in subject.

    topicARN := ""
    p := sns.NewPublisher()
    if err := p.Publish(topicARN, "Hello World", "hello"); err != nil {
      // log error
    }

##### PublishMessage

Publish publishes a Message to the Topic ARN. 

    topicARN := "arn:"

    comment := struct {
        Id      string
        Comment string
    }{"123", "Great Picture"}

    msg := Message {
        UserID  : "Some User ID",
        Subject : "Message Subject",
        Payload : comment,
    }

    p := sns.NewPublisher()
    if err := p.PublishMessage(topicARN, &msg); err != nil {
        // log error
    }