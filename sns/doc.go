// Package sns is used to interface with Amazon SNS Service. It is primarily used
// to subscribe and unscribe to a topic within Amazon SNS Service.
package sns
